## [1.0.4](https://gitlab.com/web-hook-remotejob/api/compare/v1.0.3...v1.0.4) (2023-01-25)


### Bug Fixes

* realise ([aa0f3de](https://gitlab.com/web-hook-remotejob/api/commit/aa0f3dea2099c6d907099e60d6b835f8080d4379))

FROM python:3.8-alpine as base
WORKDIR /app

FROM base as testing
COPY tests .
RUN pip install -r requirements.txt
CMD ["pytest", "-s"] 

FROM base as build
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .

FROM build as dev
ENV FLASK_DEBUG=1
ENV FLASK_ENV=development
EXPOSE 5000
CMD ["flask", "run", "--host=0.0.0.0"]

FROM build as production
EXPOSE 5000
CMD ["gunicorn", "-w=4", "--bind=0.0.0.0:5000", "wsgi:app", "--keep-alive=0"]
This microservice expose an HTTP api and communicate with the underlying database to:
- create new webhook endpoint (dedicated token)
- create data
- publish each piece of data to the underlying NATS message broker

# DOC
https://github.com/semantic-release/gitlab

https://docs.gitlab.com/ee/ci/examples/semantic-release.html

EXAMPLES: commit msg
fix: testing patch releases

feat: testing major releases

BREAKING CHANGE: This is a breaking change.
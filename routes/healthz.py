from flask import request, jsonify, g
from . import routes
import logging


@routes.route('/healthz', methods=['GET'])
def healthz():
    return jsonify({}), 200

from flask import request, jsonify, g
from lib.db import Database
from . import routes
import datetime
import time
import logging


@routes.route('/readyz', methods=['GET'])
def readyz():
    # Check if api can create/update an healthcheck entry in the Database
    try:
        Database.upsert("healthcheck", { "service": "api" }, {"ts": datetime.datetime.now().astimezone().isoformat()})
    except Exception as e:
        logging.error("cannot set healthcheck entry in db")
        return jsonify({}), 500
    return jsonify({}), 200
